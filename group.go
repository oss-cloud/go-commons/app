package app

import (
	"context"
	"sync"

	"go.uber.org/multierr"
	"go.uber.org/zap"
)

type Group struct {
	Apps   []App
	Logger *zap.Logger
}

func (g *Group) Start() error {
	logger := g.Logger.With(
		zap.String("method", "start"),
	)
	for _, app := range g.Apps {
		go func(app App) {
			logger.Info("starting app", zap.String("name", app.Name()))
			app.Start()
		}(app)
	}
	return nil
}

func (g *Group) Shutdown(ctx context.Context) error {
	logger := g.Logger.With(
		zap.String("method", "shutdown"),
	)

	var (
		wg   sync.WaitGroup
		errs error = nil
	)
	for _, app := range g.Apps {
		wg.Add(1)
		go func(app App) {
			defer wg.Done()
			logger.Info("starting app", zap.String("name", app.Name()))
			if err := app.Shutdown(ctx); err != nil {
				errs = multierr.Append(errs, err)
			}
		}(app)
	}

	wg.Wait()

	return errs
}
