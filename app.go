package app

import "context"

type App interface {
	Name() string
	Start() error
	Shutdown(ctx context.Context) error
}
